package com.example.hp.rapphim.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ViewFlipper;

import com.example.hp.rapphim.R;
import com.example.hp.rapphim.View.Menuview;
import com.example.hp.rapphim.util.CheckConnection;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements Menuview.MenuviewListener {


    ViewFlipper viewFlipper;
    ListView listViewmanhinhchinh;
    Menuview menuview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Anhxa();
        if(CheckConnection.haveNetworkConnection(getApplicationContext())){
            ActionViewFlipper();
        }else{
            CheckConnection.showToast_Short(getApplicationContext(),"Bạn hãy kiểm tra lại kết nối");
        }
    }






    public void ActionViewFlipper(){
        ArrayList<String> mangquangcao=new ArrayList<>();
        mangquangcao.add("https://www.galaxycine.vn/media/wysiwyg/images/phim%20hay%20thang%20/thang%208%202015/MISSION%20IMPOSSIBLE%20banner'.jpg");
        mangquangcao.add("https://www.galaxycine.vn/media/wysiwyg/images/phim%20hay%20thang%20/thang%208%202015/fantastic-4-banner.jpg");
        mangquangcao.add("https://www.galaxycine.vn/media/wysiwyg/images/phim%20hay%20thang%20/thang%208%202015/vacation-banner-NHE.jpg");
        mangquangcao.add("https://www.galaxycine.vn/media/wysiwyg/images/phim%20hay%20thang%20/thang%208%202015/The-Man-From-U-N-C-L-E-banner-file-nhe.jpg");

        for (int i=0;i<mangquangcao.size();i++){
            ImageView imageView=new ImageView(getApplicationContext());
            Picasso.with(getApplicationContext()).load(mangquangcao.get(i)).into(imageView);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            viewFlipper.addView(imageView);
        }

        viewFlipper.setFlipInterval(5000);
        viewFlipper.setAutoStart(true);

    }
    public void Anhxa(){

        viewFlipper=(ViewFlipper) findViewById(R.id.viewflipper);
        listViewmanhinhchinh=(ListView) findViewById(R.id.listviewmanhinh);
        menuview= findViewById(R.id.menuview);
        menuview.setmListener(this);
    }

    @Override
    public void onTrangchu() {

    }

    @Override
    public void onLichchieu() {
        Intent mIntent = new Intent(this, LichchieuActivity.class);
        startActivity(mIntent);
    }

    @Override
    public void onDienanh() {
        Intent mIntent = new Intent(this, TintucActivity.class);
        startActivity(mIntent);

    }

    @Override
    public void onTaikhoan() {
        Intent mIntent = new Intent(this, TaikhoanActivity.class);
        startActivity(mIntent);
    }
}
