package com.example.hp.rapphim.model;

import java.io.Serializable;
import java.util.Date;

public class Phim implements Serializable {
    int id;
    String tenphim;
    String hinhanh;
    String mota;
    int giave;
    String ngaychieu;


    public Phim(int id, String tenphim, String hinhanh, String mota, int giave, String ngaychieu) {
        this.id = id;
        this.tenphim = tenphim;
        this.hinhanh = hinhanh;
        this.mota = mota;
        this.giave = giave;
        this.ngaychieu = ngaychieu;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTenphim() {
        return tenphim;
    }

    public void setTenphim(String tenphim) {
        this.tenphim = tenphim;
    }

    public String getHinhanh() {
        return hinhanh;
    }

    public void setHinhanh(String hinhanh) {
        this.hinhanh = hinhanh;
    }

    public String getMota() {
        return mota;
    }

    public void setMota(String mota) {
        this.mota = mota;
    }

    public int getGiave() {
        return giave;
    }

    public void setGiave(int giave) {
        this.giave = giave;
    }

    public String getNgaychieu() {
        return ngaychieu;
    }

    public void setNgaychieu(String ngaychieu) {
        this.ngaychieu = ngaychieu;
    }
}
