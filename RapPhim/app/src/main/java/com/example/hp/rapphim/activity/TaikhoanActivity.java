package com.example.hp.rapphim.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.hp.rapphim.R;
import com.example.hp.rapphim.View.Menuview;
import com.example.hp.rapphim.util.CheckConnection;

public class TaikhoanActivity extends AppCompatActivity implements Menuview.MenuviewListener {

    TextView textViewdangnhap,textViewdangky;
    Menuview menuview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taikhoan);
        Anhxa();
        if(CheckConnection.haveNetworkConnection(getApplicationContext())){
            CatchOnTextview();
        }else{
            CheckConnection.showToast_Short(getApplicationContext(),"Bạn hãy kiểm tra lại kết nối");
        }
    }

    private void CatchOnTextview() {
        textViewdangnhap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TaikhoanActivity.this,Dangnhap.class);
                startActivity(intent);
            }
        });
        textViewdangky.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TaikhoanActivity.this,Dangky.class);
                startActivity(intent);
            }
        });
    }


    private void Anhxa() {
        textViewdangnhap=(TextView) findViewById(R.id.dangnhap);
        textViewdangky=(TextView) findViewById(R.id.dangky);
        menuview=findViewById(R.id.menuview);
        menuview.setmListener(this);

    }

    @Override
    public void onTrangchu() {
        Intent mIntent = new Intent(this, MainActivity.class);
        startActivity(mIntent);
    }

    @Override
    public void onLichchieu() {
        Intent mIntent = new Intent(this, LichchieuActivity.class);
        startActivity(mIntent);
    }

    @Override
    public void onDienanh() {
        Intent mIntent = new Intent(this, TintucActivity.class);
        startActivity(mIntent);
    }

    @Override
    public void onTaikhoan() {

    }
}
