package com.example.hp.rapphim.View;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.example.hp.rapphim.R;

public class Menuview extends LinearLayout implements View.OnClickListener {
    LinearLayout layouttrangchu, layoutlichchieu,layoutdienanh,layouttaikhoan;
    MenuviewListener mListener;

    public void setmListener(MenuviewListener mListener) {
        this.mListener = mListener;
    }

    public interface MenuviewListener {
        public void onTrangchu();
        public void onLichchieu();
        public void onDienanh();
        public void onTaikhoan();
    }

    public Menuview(Context context) {
        super(context);
        InitView(context);
    }

    public Menuview(Context context, AttributeSet attrs) {
        super(context, attrs);
        InitView(context);
    }
    public void InitView(Context context){
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.footer, this,true);
        layouttrangchu=(LinearLayout) findViewById(R.id.trangchu);
        layoutlichchieu=(LinearLayout) findViewById(R.id.lichchieu);
        layoutdienanh=(LinearLayout) findViewById(R.id.dienanh);
        layouttaikhoan=(LinearLayout) findViewById(R.id.taikhoan);
        layouttrangchu.setOnClickListener(this);
        layoutlichchieu.setOnClickListener(this);
        layoutdienanh.setOnClickListener(this);
        layouttaikhoan.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.trangchu:
                mListener.onTrangchu();
                break;
            case R.id.lichchieu:
                mListener.onLichchieu();
                break;
            case R.id.taikhoan:
                mListener.onTaikhoan();
                break;
            case R.id.dienanh:
                mListener.onDienanh();
                break;

        }

    }
}
