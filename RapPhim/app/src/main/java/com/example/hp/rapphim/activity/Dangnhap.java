package com.example.hp.rapphim.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.hp.rapphim.R;
import com.example.hp.rapphim.util.CheckConnection;

public class Dangnhap extends AppCompatActivity {

    Toolbar toolbardn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dangnhap);
        Anhxa();
        if(CheckConnection.haveNetworkConnection(getApplicationContext())){
            ActionToolbar();

        }else{
            CheckConnection.showToast_Short(getApplicationContext(),"Bạn hãy kiểm tra lại kết nối");
        }
    }


    private void ActionToolbar() {
        setSupportActionBar(toolbardn);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbardn.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void Anhxa() {

        toolbardn=(Toolbar) findViewById(R.id.tbdangnhap);
    }
}
