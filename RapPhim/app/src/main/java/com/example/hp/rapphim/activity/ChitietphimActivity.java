package com.example.hp.rapphim.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.hp.rapphim.R;
import com.example.hp.rapphim.View.Menuview;
import com.example.hp.rapphim.util.CheckConnection;

public class ChitietphimActivity extends AppCompatActivity implements Menuview.MenuviewListener {

    Toolbar toolbarctphim;
    Menuview menuview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chitietphim);
        Anhxa();
        if(CheckConnection.haveNetworkConnection(getApplicationContext())){
            ActionToolbar();
        }else{
            CheckConnection.showToast_Short(getApplicationContext(),"Bạn hãy kiểm tra lại kết nối");
        }
    }


    private void ActionToolbar() {
        setSupportActionBar(toolbarctphim);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarctphim.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void Anhxa() {
        toolbarctphim=(Toolbar) findViewById(R.id.idchitietphim);
        menuview=findViewById(R.id.menuview);
        menuview.setmListener(this);
    }

    @Override
    public void onTrangchu() {
        Intent mIntent = new Intent(this, MainActivity.class);
        startActivity(mIntent);
    }

    @Override
    public void onLichchieu() {
        Intent mIntent = new Intent(this, LichchieuActivity.class);
        startActivity(mIntent);
    }

    @Override
    public void onDienanh() {
        Intent mIntent = new Intent(this, TintucActivity.class);
        startActivity(mIntent);
    }

    @Override
    public void onTaikhoan() {
        Intent mIntent = new Intent(this, TaikhoanActivity.class);
        startActivity(mIntent);
    }
}
