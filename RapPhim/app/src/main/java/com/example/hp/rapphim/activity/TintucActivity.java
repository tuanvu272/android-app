package com.example.hp.rapphim.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.support.v7.widget.Toolbar;

import com.example.hp.rapphim.R;
import com.example.hp.rapphim.View.Menuview;
import com.example.hp.rapphim.util.CheckConnection;

public class TintucActivity extends AppCompatActivity implements Menuview.MenuviewListener {
    Toolbar toolbar;
    ListView listView;
    Menuview menuview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tintuc);
        Anhxa();
        if(CheckConnection.haveNetworkConnection(getApplicationContext())){

        }else {
            CheckConnection.showToast_Short(getApplicationContext(),"Bạn hãy kiểm tra lại kết nối");
        }
    }


    public void Anhxa(){

        listView=(ListView) findViewById(R.id.listviewdienanh);
        menuview=findViewById(R.id.menuview);
        menuview.setmListener(this);

    }

    @Override
    public void onTrangchu() {
        Intent mIntent = new Intent(this, MainActivity.class);
        startActivity(mIntent);
    }

    @Override
    public void onLichchieu() {
        Intent mIntent = new Intent(this, LichchieuActivity.class);
        startActivity(mIntent);
    }

    @Override
    public void onDienanh() {

    }

    @Override
    public void onTaikhoan() {
        Intent mIntent = new Intent(this, TaikhoanActivity.class);
        startActivity(mIntent);
    }
}
