package com.example.hp.rapphim.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.hp.rapphim.R;
import com.example.hp.rapphim.View.Menuview;
import com.example.hp.rapphim.util.CheckConnection;

public class LichchieuActivity extends AppCompatActivity implements Menuview.MenuviewListener {
    ListView lvlichchieu;
    Menuview menuview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lichchieu);
        Anhxa();
        if(CheckConnection.haveNetworkConnection(getApplicationContext())){

        }else{
            CheckConnection.showToast_Short(getApplicationContext(),"Bạn hãy kiểm tra lại kết nối");
        }
    }





    private void Anhxa() {
        lvlichchieu=(ListView) findViewById(R.id.listviewlichchieu);
        menuview=findViewById(R.id.menuview);
        menuview.setmListener(this);
    }

    @Override
    public void onTrangchu() {
        Intent mIntent = new Intent(this, MainActivity.class);
        startActivity(mIntent);
    }

    @Override
    public void onLichchieu() {

    }

    @Override
    public void onDienanh() {
        Intent mIntent = new Intent(this, TintucActivity.class);
        startActivity(mIntent);
    }

    @Override
    public void onTaikhoan() {
        Intent mIntent = new Intent(this, TaikhoanActivity.class);
        startActivity(mIntent);
    }
}
